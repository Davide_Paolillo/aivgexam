﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPreview : MonoBehaviour
{
    [Header("Map Settings")]
    [Range(0, MeshSettings.numberOfSupportedLevelOfDetails - 1)] [SerializeField] private int editorLevelOfSimplification;
    [SerializeField] private DrawMode drawMode;

    [Header("Terrain Settings")]
    [SerializeField] private MeshSettings meshSettings;

    [Header("Texture Settings")]
    [SerializeField] private TextureData textureData;
    [SerializeField] private Material terrainMaterial;

    [Header("Noise Settings")]
    [SerializeField] private HeightMapSettings heightMapSettings;

    [Header("Editor Settings")]
    [SerializeField] private bool autoUpdate;

    [Header("Mesh Settings")]
    [SerializeField] private Renderer textureRenderer;
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private MeshRenderer meshRenderer;

    public bool AutoUpdate { get => autoUpdate; }


    public MeshSettings GetMeshSettings { get => meshSettings; }

    private void OnTextureValuesUpdated()
    {
        textureData.ApplyToMaterial(terrainMaterial);
    }

    private void OnValuesUpdated()
    {
        if (!Application.isPlaying)
            DrawMap();
    }

    private void OnValidate()
    {
        if (meshSettings != null)
        {
            meshSettings.OnValuesUpdated -= OnValuesUpdated;
            meshSettings.OnValuesUpdated += OnValuesUpdated;
        }

        if (heightMapSettings != null)
        {
            heightMapSettings.OnValuesUpdated -= OnValuesUpdated;
            heightMapSettings.OnValuesUpdated += OnValuesUpdated;
        }

        if (textureData != null)
        {
            textureData.OnValuesUpdated -= OnTextureValuesUpdated;
            textureData.OnValuesUpdated += OnTextureValuesUpdated;
        }
    }

    public void DrawMap()
    {
        textureData.ApplyToMaterial(terrainMaterial);
        textureData.UpdateMeshHeights(terrainMaterial, heightMapSettings.MinHeight, heightMapSettings.MaxHeight);

        HeightMap heightMap = HeightMapGenerator.GenerateHeightMap(meshSettings.NumberOfVerticesPerLine, meshSettings.NumberOfVerticesPerLine, heightMapSettings, Vector2.zero);

        switch (drawMode)
        {
            case DrawMode.NOISE_MAP:
                DrawNoiseMap(heightMap);
                break;
            case DrawMode.FALLOFF_MAP:
                DrawFalloffMap(heightMap, meshSettings.NumberOfVerticesPerLine);
                break;
            case DrawMode.MESH:
                GenerateMapMesh(MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, editorLevelOfSimplification));
                break;
            default:
                DrawNoiseMap(heightMap);
                break;
        }
    }

    public void DrawNoiseMap(HeightMap heightMap)
    {
        Texture2D texture = TextureGenerator.TextureFromHeightMap(heightMap);

        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(heightMap.values.GetLength(0), 1, heightMap.values.GetLength(1)) / 10f;

        textureRenderer.gameObject.SetActive(true);
        meshFilter.gameObject.SetActive(false);
    }
    
    public void DrawFalloffMap(HeightMap heightMap, int numberOfVerticesPerLine)
    {
        Texture2D texture = TextureGenerator.TextureFromHeightMap(new HeightMap(FalloffGenerator.GenerateFalloffMap(numberOfVerticesPerLine), 0, 1));

        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(heightMap.values.GetLength(0), 1, heightMap.values.GetLength(1)) / 10f;
        
        textureRenderer.gameObject.SetActive(true);
        meshFilter.gameObject.SetActive(false);
    }
    
    public void GenerateMapMesh(MeshData meshData)
    {
        meshFilter.sharedMesh = meshData.CreateMesh();

        textureRenderer.gameObject.SetActive(false);
        meshFilter.gameObject.SetActive(true);
    }
}
