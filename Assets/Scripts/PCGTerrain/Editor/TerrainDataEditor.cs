﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HeightMapSettings))]
public class TerrainDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var heightMapSettings = target as HeightMapSettings;

        if (heightMapSettings.applyFalloffMask)
        {
            heightMapSettings.paddingFalloffMaskFactorA = EditorGUILayout.Slider("Padding Falloff Mask Factor A", heightMapSettings.paddingFalloffMaskFactorA, 0.0f, 100.0f);
            heightMapSettings.paddingFalloffMaskFactorB = EditorGUILayout.Slider("Padding Falloff Mask Factor B", heightMapSettings.paddingFalloffMaskFactorB, 0.0f, 100.0f);
        }

        if (GUILayout.Button("Update"))
            heightMapSettings.NotifyOfUpdatedValues();
    }
}
