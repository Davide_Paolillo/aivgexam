﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadedDataRequestor : MonoBehaviour
{
    Queue<MapThreadInfo> mapDataThreadInfoQueue;
    Queue<MapThreadInfo> meshDataThreadInfoQueue;

    private static ThreadedDataRequestor instance;

    private void Awake()
    {
        instance = FindObjectOfType<ThreadedDataRequestor>();
        mapDataThreadInfoQueue = new Queue<MapThreadInfo>();
        meshDataThreadInfoQueue = new Queue<MapThreadInfo>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    public static void RequestMapData(Func<object> generateData, Action<object> callback)
    {
        ThreadStart threadStart = delegate
        {
            instance.MapDataThread(generateData, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MapDataThread(Func<object> generateData, Action<object> callback)
    {
        object data = generateData();

        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo(callback, data));
        }
    }

    public void RequestMeshData(HeightMap mapData, MeshSettings meshSettings, int levelOfSimplification, Action<object> callback)
    {
        ThreadStart threadStart = delegate
        {
            MeshDataThread(mapData, meshSettings, levelOfSimplification, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MeshDataThread(HeightMap heightMap, MeshSettings meshSettings, int levelOfSimplification, Action<object> callback)
    {
        MeshData meshData = MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, levelOfSimplification);

        lock (meshDataThreadInfoQueue)
        {
            meshDataThreadInfoQueue.Enqueue(new MapThreadInfo(callback, meshData));
        }
    }

    private void Update()
    {
        if (mapDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }

        if (meshDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo threadInfo = meshDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
    }
}
