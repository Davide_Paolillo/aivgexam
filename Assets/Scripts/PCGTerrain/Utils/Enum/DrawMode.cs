﻿public enum DrawMode
{
    NOISE_MAP,
    FALLOFF_MAP,
    MESH
}
