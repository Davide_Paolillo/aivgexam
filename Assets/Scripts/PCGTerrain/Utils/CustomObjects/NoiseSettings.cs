﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoiseSettings
{
    public int seed;
    public float scale = 50;
    public int octaves = 6;
    public float lacunarity = 2f;
    [Range(0.0f, 1.0f)] public float persistance = .6f;

    public Vector2 startingOffset;
    public float normalizeFactor = 1.8f;
    public NormalizeMode normalizeMode;

    public void ValidateValues()
    {
        scale = Mathf.Max(scale, 0.15f);
        octaves = Mathf.Clamp(octaves, 1, 30);
        lacunarity = Mathf.Max(lacunarity, 1);
        persistance = Mathf.Clamp01(persistance);
    }
}
