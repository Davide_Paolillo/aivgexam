﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TerrainLayer
{
    public Texture2D texture;
    public Color tint;
    [Range(0f, 1f)] public float tintStrength;
    [Range(0f, 1f)] public float startHeight;
    [Range(0f, 1f)] public float blendStrength;
    public float textureScale;
}
