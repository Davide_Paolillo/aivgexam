﻿using UnityEngine;

public class LevelOfDetailMesh
{
    private Mesh mesh;
    private bool hasRequestedMesh;
    private bool hasMesh;

    private int levelOfDetail;

    public event System.Action updateCallback;

    public Mesh Mesh { get => mesh; }
    public bool HasRequestedMesh { get => hasRequestedMesh; }
    public bool HasMesh { get => hasMesh; }

    public LevelOfDetailMesh(int levelOfDetail)
    {
        this.levelOfDetail = levelOfDetail;
    }

    private void OnMeshDataRecieved(object meshData)
    {
        mesh = ((MeshData)meshData).CreateMesh();
        hasMesh = true;

        updateCallback();
    }

    public void RequestMesh(HeightMap heightMap, MeshSettings meshSettings)
    {
        hasRequestedMesh = true;
        ThreadedDataRequestor.RequestMapData(() => MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, levelOfDetail), OnMeshDataRecieved);
    }
}
