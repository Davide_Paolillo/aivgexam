﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChunk
{
    public event System.Action<TerrainChunk, bool> OnVisibilityChanged;

    private Vector2 coordinates;

    private GameObject meshObject;

    private Vector2 sampleCenter;

    private Bounds bounds;

    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private MeshCollider meshCollider;

    private LevelOfDetailInfo[] detailLevels;
    private LevelOfDetailMesh[] levelOfDetailMeshes;
    // private LevelOfDetailMesh collisionLevelOfDetailMesh;
    private int colliderLevelOfDetailIndex;

    private HeightMap heightMap;
    private bool heightMapRecieved;

    private int previousLevelOfDetailIndex;

    private bool hasSetCollider;

    private HeightMapSettings heightMapSettings;
    private MeshSettings meshSettings;

    public Vector2 Coordinates { get => coordinates; set => coordinates = value; }
    public GameObject MeshObject { get => meshObject; }
    public Bounds Bounds { get => bounds; }
    public HeightMap HeightMap { get => heightMap; }
    public MeshFilter MeshFilter { get => meshFilter; }
    public MeshCollider MeshCollider { get => meshCollider; }

    public TerrainChunk(Vector2 coordinate, HeightMapSettings heightMapSettings, MeshSettings meshSettings,
        LevelOfDetailInfo[] detailLevels, int colliderLevelOfSimplificationIndex, Transform parent, Material mapMaterial, float scale)
    {
        this.coordinates = coordinate;
        this.previousLevelOfDetailIndex = -1;
        this.colliderLevelOfDetailIndex = colliderLevelOfSimplificationIndex;
        this.hasSetCollider = false;
        this.heightMapSettings = heightMapSettings;
        this.meshSettings = meshSettings;

        sampleCenter = coordinate * meshSettings.MeshWorldSize / meshSettings.meshScale;
        Vector2 position = coordinate * meshSettings.MeshWorldSize;
        bounds = new Bounds(position, Vector2.one * meshSettings.MeshWorldSize);

        meshObject = new GameObject("Terrain Chunk");
        meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshFilter = meshObject.AddComponent<MeshFilter>();
        meshCollider = meshObject.AddComponent<MeshCollider>();
        meshRenderer.material = mapMaterial;

        meshObject.transform.position = new Vector3(position.x, 0, position.y);
        meshObject.transform.parent = parent;
        SetVisible(false);

        this.detailLevels = detailLevels;

        levelOfDetailMeshes = new LevelOfDetailMesh[detailLevels.Length];

        for (int i = 0; i < detailLevels.Length; i++)
        {
            levelOfDetailMeshes[i] = new LevelOfDetailMesh(detailLevels[i].levelOfDetail);
            levelOfDetailMeshes[i].updateCallback += UpdateTerrainChunk;

            if (i == colliderLevelOfSimplificationIndex)
                levelOfDetailMeshes[i].updateCallback += UpdateCollisionMesh;
        }
    }

    public void Load()
    {
        ThreadedDataRequestor.RequestMapData(() => HeightMapGenerator.GenerateHeightMap(meshSettings.NumberOfVerticesPerLine, meshSettings.NumberOfVerticesPerLine, heightMapSettings, sampleCenter),
            OnHeightMapRecieved);
    }

    private void OnHeightMapRecieved(object mapData)
    {
        this.heightMap = (HeightMap)mapData;
        heightMapRecieved = true;

        UpdateTerrainChunk();
    }

    public void UpdateTerrainChunk()
    {
        if (!heightMapRecieved)
            return;

        bool wasVisible = IsVisible();
        float playerDistanceFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(TerrainGenerator.PlayerPosition));
        bool visible = playerDistanceFromNearestEdge <= TerrainGenerator.MaxViewDistance;

        // Loop through all the current inspected chunk and see how far is it from us, this gives us the chunk detail level, we do not check the last cos if it is grater than it means that this chunk won't be visible
        if (visible)
        {
            int levelOfDetailIndex = 0;

            for (int i = 0; i < detailLevels.Length - 1; i++)
            {
                if (playerDistanceFromNearestEdge > detailLevels[i].visibleDistanceThreshold)
                    levelOfDetailIndex = i + 1;
                else
                    break;
            }

            if (levelOfDetailIndex != previousLevelOfDetailIndex)
            {
                LevelOfDetailMesh levelOfDetailMesh = levelOfDetailMeshes[levelOfDetailIndex];

                if (levelOfDetailMesh.HasMesh)
                {
                    previousLevelOfDetailIndex = levelOfDetailIndex;
                    meshFilter.mesh = levelOfDetailMesh.Mesh;
                }
                else if (!levelOfDetailMesh.HasRequestedMesh)
                {
                    levelOfDetailMesh.RequestMesh(heightMap, meshSettings);
                }
            }
        }

        if (wasVisible != visible)
        {
            SetVisible(visible);

            if (OnVisibilityChanged != null)
                OnVisibilityChanged(this, visible);
        }

    }

    public void UpdateCollisionMesh()
    {
        if (hasSetCollider)
            return;

        float sqrDistanceFromViewerToEdge = bounds.SqrDistance(TerrainGenerator.PlayerPosition);

        if (sqrDistanceFromViewerToEdge < detailLevels[colliderLevelOfDetailIndex].sqrVisibleDistanceThreshold)
            if (!levelOfDetailMeshes[colliderLevelOfDetailIndex].HasRequestedMesh)
                levelOfDetailMeshes[colliderLevelOfDetailIndex].RequestMesh(heightMap, meshSettings);

        if (sqrDistanceFromViewerToEdge < TerrainGenerator.ColliderGenerationDistanceThreshold * TerrainGenerator.ColliderGenerationDistanceThreshold)
            if (levelOfDetailMeshes[colliderLevelOfDetailIndex].HasMesh)
            {
                meshCollider.sharedMesh = levelOfDetailMeshes[colliderLevelOfDetailIndex].Mesh;
                hasSetCollider = true;
            }
    }

    public void SetVisible(bool visible)
    {
        meshObject.SetActive(visible);
    }

    public bool IsVisible()
    {
        return meshObject.activeSelf;
    }
}
