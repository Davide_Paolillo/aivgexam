﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshData
{
    private Vector3[] vertices;
    private int[] triangles;
    private Vector2[] uvs;
    private Vector3[] bakedNormals;

    private Vector3[] outOfMeshVertices;
    private int[] outOfMeshTriangles;

    private int triangleIndex;
    private int outOfMeshTriangleIndex;

    private bool useFlatShading;

    public MeshData(int numberOfVerticesPerLine, int meshSimplificationIncrement, bool useFlatShading)
    {
        int numberOfMeshEdgeVertices = (numberOfVerticesPerLine - 2) * 4 - 4;
        int numberOfEdgeConnectionVertices = (meshSimplificationIncrement - 1) * (numberOfVerticesPerLine - 5) / meshSimplificationIncrement * 4;
        int numberOfMainVerticesPerLine = (numberOfVerticesPerLine - 5) / meshSimplificationIncrement + 1;
        int numberOfMainVertices = numberOfMainVerticesPerLine * numberOfMainVerticesPerLine;

        vertices = new Vector3[numberOfMeshEdgeVertices + numberOfEdgeConnectionVertices + numberOfMainVertices];
        uvs = new Vector2[vertices.Length];

        int numberOfMeshEdgeTriangles = 8 * (numberOfVerticesPerLine - 4);
        int numberOfMainTriangles = (numberOfMainVerticesPerLine-1) * (numberOfMainVerticesPerLine - 1) * 2;
        triangles = new int[(numberOfMeshEdgeTriangles + numberOfMainTriangles) * 3];

        outOfMeshVertices = new Vector3[numberOfVerticesPerLine * 4 - 4];
        outOfMeshTriangles = new int[24 * (numberOfVerticesPerLine - 2)];

        triangleIndex = 0;
        outOfMeshTriangleIndex = 0;

        this.useFlatShading = useFlatShading;
    }

    public Vector3[] Vertices { get => vertices; set => vertices = value; }
    public int[] Triangles { get => triangles; set => triangles = value; }
    public Vector2[] Uvs { get => uvs; set => uvs = value; }

    public void AddVertex(Vector3 vertexPosition, Vector2 uv, int vertexIndex)
    {
        if (vertexIndex < 0)
        {
            outOfMeshVertices[Mathf.Abs(vertexIndex) - 1] = vertexPosition;
        }
        else
        {
            vertices[vertexIndex] = vertexPosition;
            uvs[vertexIndex] = uv;
        }
    }

    public void AddTriangle(int firstVertex, int secondVertex, int thirdVertex)
    {
        if (firstVertex < 0 || secondVertex < 0 || thirdVertex < 0)
        {
            outOfMeshTriangles[outOfMeshTriangleIndex] = firstVertex;
            outOfMeshTriangles[outOfMeshTriangleIndex + 1] = secondVertex;
            outOfMeshTriangles[outOfMeshTriangleIndex + 2] = thirdVertex;

            outOfMeshTriangleIndex += 3;
        }
        else 
        { 
            triangles[triangleIndex] = firstVertex;
            triangles[triangleIndex + 1] = secondVertex;
            triangles[triangleIndex + 2] = thirdVertex;

            triangleIndex += 3;
        }
    }

    // Using this method to claculate normals over the edge of the adjacent chunks that like this are screwed up due to the fact that each chunk does not have the acess to the adjacent chunks normals
    private Vector3[] CalculateNormals()
    {
        Vector3[] vertexNormals = new Vector3[vertices.Length];
        int triangleCount = triangles.Length / 3;

        for (int i = 0; i < triangleCount; i++)
        {
            int normalTriangleIndex = i * 3;
            int vertexIndexA = triangles[normalTriangleIndex];
            int vertexIndexB = triangles[normalTriangleIndex + 1];
            int vertexIndexC = triangles[normalTriangleIndex + 2];

            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
            vertexNormals[vertexIndexA] += triangleNormal;
            vertexNormals[vertexIndexB] += triangleNormal;
            vertexNormals[vertexIndexC] += triangleNormal;
        }
        
        int borderTriangleCount = outOfMeshTriangles.Length / 3;

        for (int i = 0; i < borderTriangleCount; i++)
        {
            int normalTriangleIndex = i * 3;
            int vertexIndexA = outOfMeshTriangles[normalTriangleIndex];
            int vertexIndexB = outOfMeshTriangles[normalTriangleIndex + 1];
            int vertexIndexC = outOfMeshTriangles[normalTriangleIndex + 2];

            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);

            if (vertexIndexA > 0) 
                vertexNormals[vertexIndexA] += triangleNormal;
            if (vertexIndexB > 0)
                vertexNormals[vertexIndexB] += triangleNormal;
            if (vertexIndexC > 0)
                vertexNormals[vertexIndexC] += triangleNormal;
        }

        for (int i = 0; i < vertexNormals.Length; i++)
        {
            vertexNormals[i].Normalize();
        }

        return vertexNormals;
    }

    private Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC)
    {
        Vector3 pointA = indexA < 0 ? outOfMeshVertices[Mathf.Abs(indexA) - 1] : vertices[indexA];
        Vector3 pointB = indexB < 0 ? outOfMeshVertices[Mathf.Abs(indexB) - 1] : vertices[indexB];
        Vector3 pointC = indexC < 0 ? outOfMeshVertices[Mathf.Abs(indexC) - 1] : vertices[indexC];

        Vector3 sideAB = pointB - pointA;
        Vector3 sideAC = pointC - pointA;

        return Vector3.Cross(sideAB, sideAC).normalized;
    }

    /// <summary>
    /// Call this method in a separate thread in order to prevent stuttering phases in the normals calculations
    /// </summary>
    private void BakeNormals()
    {
        bakedNormals = CalculateNormals();
    }

    /// <summary>
    /// Create triangles without shared vertices, in order to assign to each unique triangle his normals, with this we will not use an average value of the normal of the shared vertices between adjacent triangles,
    /// instead we'll have each unique triangle illuminated based on each of his vertex normal (instead of the average). This will give to the map a nice "Flat shaded" effect.
    /// </summary>
    private void FlatShading()
    {
        Vector3[] flatShadedVertices = new Vector3[triangles.Length];
        Vector2[] flatShadedUvs = new Vector2[triangles.Length];

        for (int i = 0; i < triangles.Length; i++)
        {
            flatShadedVertices[i] = vertices[triangles[i]];
            flatShadedUvs[i] = uvs[triangles[i]];
            // Assigning to each triangle a new index based on the triangle array length
            triangles[i] = i;
        }

        vertices = flatShadedVertices;
        uvs = flatShadedUvs;
    }

    public void BakeMeshNormals()
    {
        if (useFlatShading)
            FlatShading();
        else
            BakeNormals();
    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        if (useFlatShading)
            mesh.RecalculateNormals();
        else
            mesh.normals = bakedNormals;

        return mesh;
    }
}
