﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    public static MeshData GenerateTerrainMesh(float[,] heightMap, MeshSettings meshSettings, int levelOfSimplification)
    {
        // We should consider it in a scale from 0 to 12 with steps of 2, due to the formula that lays underneath, this allow us to render less vertices when we want a bigger map rendered to the screen
        int meshSimplificationIncrement = levelOfSimplification > 0 ? levelOfSimplification * 2 : 1;
        int numberOfVerticesPerLine = meshSettings.NumberOfVerticesPerLine;

        // To center the first vertex in the middle of the mesh, in order to have coordinates that are both + and -
        Vector2 topLeft = new Vector2(-1, 1) * meshSettings.MeshWorldSize / 2f;

        MeshData meshData = new MeshData(numberOfVerticesPerLine, meshSimplificationIncrement, meshSettings.useFlatShading);

        int[,] vertexIndicesMap = new int[numberOfVerticesPerLine, numberOfVerticesPerLine];
        int meshVertexIndex = 0;
        int outOfMeshIndex = -1;

        for (int y = 0; y < numberOfVerticesPerLine; y++)
        {
            for (int x = 0; x < numberOfVerticesPerLine; x++)
            {
                bool isOutOfMeshVertex = y == 0 || y == numberOfVerticesPerLine - 1 || x == 0 || x == numberOfVerticesPerLine - 1;
                bool isSkippedVertex = IsSkippedVertex(meshSimplificationIncrement, numberOfVerticesPerLine, y, x);

                if (isOutOfMeshVertex)
                {
                    // The vertices on the edge will be numbered starting from -1 downwards
                    vertexIndicesMap[x, y] = outOfMeshIndex;
                    outOfMeshIndex--;
                }
                else if (!isSkippedVertex)
                {
                    // The mesh vertices will be numbered starting from 0 upwards
                    vertexIndicesMap[x, y] = meshVertexIndex;
                    meshVertexIndex++;
                }
            }
        }

        for (int y = 0; y < numberOfVerticesPerLine; y++)
        {
            for (int x = 0; x < numberOfVerticesPerLine; x++)
            {
                bool isSkippedVertex = IsSkippedVertex(meshSimplificationIncrement, numberOfVerticesPerLine, y, x);

                if (!isSkippedVertex)
                {
                    bool isOutOfMeshVertex = y == 0 || y == numberOfVerticesPerLine - 1 || x == 0 || x == numberOfVerticesPerLine - 1;
                    bool isMeshEdgeVertex = (y == 1 || y == numberOfVerticesPerLine - 2 || x == 1 || x == numberOfVerticesPerLine - 2) && !isOutOfMeshVertex;
                    bool isMainVertex = (x - 2) % meshSimplificationIncrement == 0 && (y - 2) % meshSimplificationIncrement == 0 && !isOutOfMeshVertex && !isMeshEdgeVertex;
                    bool isEdgeConnectionVertex = (y == 2 || y == numberOfVerticesPerLine - 3 || x == 2 || x == numberOfVerticesPerLine - 3) && !isOutOfMeshVertex && !isMeshEdgeVertex && !isMainVertex;

                    int vertexIndex = vertexIndicesMap[x, y];

                    Vector2 percent = new Vector2(x-1, y-1) / (numberOfVerticesPerLine - 3);
                    Vector2 vertexPosition2D = topLeft + new Vector2(percent.x, -percent.y) * meshSettings.MeshWorldSize;
                    float height = heightMap[x, y];

                    if (isEdgeConnectionVertex)
                    {
                        bool isVerticalVertex = x == 2 || x == numberOfVerticesPerLine - 3;
                        int distanceToMainVertexA = (isVerticalVertex ? (y - 2) : (x - 2)) % meshSimplificationIncrement;
                        int distanceToMainVertexB = meshSimplificationIncrement - distanceToMainVertexA;
                        float distancePercentFromAToB = distanceToMainVertexA / (float)meshSimplificationIncrement;

                        float heightMainVertexA = heightMap[(isVerticalVertex) ? x : x - distanceToMainVertexA, (isVerticalVertex) ? y - distanceToMainVertexA : y];
                        float heightMainVertexB = heightMap[(isVerticalVertex) ? x : x + distanceToMainVertexB, (isVerticalVertex) ? y + distanceToMainVertexB : y];

                        height = heightMainVertexA * (1 - distancePercentFromAToB) + heightMainVertexB * distancePercentFromAToB;
                    }

                    meshData.AddVertex(new Vector3(vertexPosition2D.x, height, vertexPosition2D.y), percent, vertexIndex);

                    bool createTriangle = x < numberOfVerticesPerLine - 1 && y < numberOfVerticesPerLine - 1 && (!isEdgeConnectionVertex || (x != 2 && y != 2));

                    if (createTriangle)
                    {
                        int currentIncrement = (isMainVertex && x != numberOfVerticesPerLine - 3 && y != numberOfVerticesPerLine-3) ? meshSimplificationIncrement : 1;

                        int vertexA = vertexIndicesMap[x, y];
                        int vertexB = vertexIndicesMap[x + currentIncrement, y];
                        int vertexC = vertexIndicesMap[x, y + currentIncrement];
                        int vertexD = vertexIndicesMap[x + currentIncrement, y + currentIncrement];
                        meshData.AddTriangle(vertexA, vertexD, vertexC);
                        meshData.AddTriangle(vertexD, vertexA, vertexB);
                    }
                }
            }
        }

        // Callin this here will prevent the calculation of the normals on the main thread, meaning that we gain performances cos we are running it on a separate thread
        meshData.BakeMeshNormals();

        return meshData;
    }

    private static bool IsSkippedVertex(int meshSimplificationIncrement, int numberOfVerticesPerLine, int y, int x)
    {
        return x > 2 && x < numberOfVerticesPerLine - 3 && y > 2 && y < numberOfVerticesPerLine - 3 && ((x - 2) % meshSimplificationIncrement != 0 || (y - 2) % meshSimplificationIncrement != 0);
    }
}
