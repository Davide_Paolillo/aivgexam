﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{

    public static float[,] GenerateNoiseMap(int mapWitdth, int mapHeight, NoiseSettings noiseSettings, Vector2 sampleCenter)
    {
        float[,] noiseMap = new float[mapWitdth, mapHeight];

        float amplitude = 1;
        float frequency = 1;

        System.Random startingPoint = new System.Random(noiseSettings.seed);
        Vector2[] octaveOffsets = new Vector2[noiseSettings.octaves];

        float maxPossibleHeight = 0;

        for (int i = 0; i < noiseSettings.octaves; i++)
        {
            float offsetX = startingPoint.Next(-100000, 100000) + noiseSettings.startingOffset.x + sampleCenter.x;
            float offsetY = startingPoint.Next(-100000, 100000) - noiseSettings.startingOffset.y - sampleCenter.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= noiseSettings.persistance;
        }

        float localMaxNoiseHeight = float.MinValue;
        float localMinNoiseHeight = float.MaxValue;


        // Calculating to zoom into the middle of the noise instead of the top right corner
        Vector2 noiseOrigin = new Vector2(mapWitdth/2f, mapHeight/2f);

        for (int y = 0; y < mapHeight; y++)
        {
            for(int x = 0; x < mapWitdth; x++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;

                for(int i = 0; i < noiseSettings.octaves; i++)
                {
                    float sampleX = (x - noiseOrigin.x + octaveOffsets[i].x) / noiseSettings.scale * frequency;
                    float sampleY = (y - noiseOrigin.y + octaveOffsets[i].y) / noiseSettings.scale * frequency;

                    // Bring the perlin value form a value of 0 to 1 ro a value between -1 and 1
                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;

                    // Generating the heightmap
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= noiseSettings.persistance;
                    frequency *= noiseSettings.lacunarity;
                }

                if (noiseHeight > localMaxNoiseHeight)
                    localMaxNoiseHeight = noiseHeight;

                if (noiseHeight < localMinNoiseHeight)
                    localMinNoiseHeight = noiseHeight;

                noiseMap[x, y] = noiseHeight;

                // This means that we have multiple chunks in a scene so we do need to estimate a minimum and a maximum height to normalize the noise map value form a value included in the range of 0 to 1 to a value from out estimated min max range
                if (noiseSettings.normalizeMode == NormalizeMode.GLOBAL)
                {
                    // Bring the height back to a value between 0 and 1 and then dividing it for the maxPossibleHeight in order to get our chunk conjunctions consistent and not detached
                    // We divide the maxPossibleHeight in order to get a maxPossibleHeight to a value that can be reached/matched by the dividend, 1.75 is and estimation
                    float normlizedHeight = (noiseMap[x, y] + 1) / (2f * (maxPossibleHeight / noiseSettings.normalizeFactor));
                    noiseMap[x, y] = normlizedHeight;
                }
            }
        }

        if (noiseSettings.normalizeMode == NormalizeMode.LOCAL)
            for (int y = 0; y < mapHeight; y++)
                for (int x = 0; x < mapWitdth; x++)
                    // This means that we generate only one chunk for each cycle and not an endless terrain, so we can consider min and max noise height chunk by chunk
                    noiseMap[x, y] = Mathf.InverseLerp(localMinNoiseHeight, localMaxNoiseHeight, noiseMap[x, y]);

        return noiseMap;
    }

}
