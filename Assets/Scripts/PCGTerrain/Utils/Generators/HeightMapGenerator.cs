﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HeightMapGenerator
{
    public static HeightMap GenerateHeightMap(int width, int height, HeightMapSettings settings, Vector2 sampleCenter)
    {
        float[,] values = Noise.GenerateNoiseMap(width, height, settings.noiseSettings, sampleCenter);

        AnimationCurve heightCurveThreadSafe = new AnimationCurve(settings.heightCurve.keys);

        float minValue = float.MaxValue;
        float maxValue = float.MinValue;

        if (settings.applyFalloffMask)
        {
            float[,] falloffMap = FalloffGenerator.GenerateFalloffMap(width, settings.paddingFalloffMaskFactorA, settings.paddingFalloffMaskFactorB);

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    values[i, j] = Mathf.Clamp01(values[i, j] - falloffMap[i, j]);
        } 

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                values[i, j] *= heightCurveThreadSafe.Evaluate(values[i, j]) * settings.heightMultiplier;

                if (values[i, j] > maxValue)
                    maxValue = values[i, j];
            
                if (values[i, j] < minValue)
                    minValue = values[i, j];
            }
        }


        return new HeightMap(values, minValue, maxValue);
    }
}
