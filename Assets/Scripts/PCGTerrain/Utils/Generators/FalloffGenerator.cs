﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator
{
    public static float[,] GenerateFalloffMap(int size, float paddingValueA = 3f, float paddingValueB = 2.2f)
    {
        float[,] map = new float[size, size];

        for (int x = 0; x < size; x++)
            for (int y = 0; y < size; y++)
            {
                float mappedXCoordinate = x / (float)size * 2 - 1;
                float mappedYCoordinate = y / (float)size * 2 - 1;

                float value = Mathf.Max(Mathf.Abs(mappedXCoordinate), Mathf.Abs(mappedYCoordinate));
                map[x, y] = Evaluate(value, paddingValueA, paddingValueB);
            }

        return map;
    }

    private static float Evaluate(float value, float a, float b)
    {
        return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - (b * value), a));
    }
}
