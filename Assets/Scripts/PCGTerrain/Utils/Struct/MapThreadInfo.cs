﻿using System;

public struct MapThreadInfo
{
    public readonly Action<object> callback;
    public readonly object parameter;

    public MapThreadInfo (Action<object> callback, object parameter)
    {
        this.callback = callback;
        this.parameter = parameter;
    }
}
