﻿using UnityEngine;

[System.Serializable]
public struct LevelOfDetailInfo
{
    [Range(0, MeshSettings.numberOfSupportedLevelOfDetails)] public int levelOfDetail;
    public int visibleDistanceThreshold;

    public float sqrVisibleDistanceThreshold
    {
        get
        {
            return visibleDistanceThreshold * visibleDistanceThreshold;
        }
    }
}