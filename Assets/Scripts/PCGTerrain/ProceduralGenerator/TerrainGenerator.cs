﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    private const float playerMovementThresholdBeforeChunkUpdate = 25f;
    private const float squaredPlayerMovementThresholdBeforeChunkUpdate = playerMovementThresholdBeforeChunkUpdate * playerMovementThresholdBeforeChunkUpdate;
    private const float colliderGenerationDistanceThreshold = 5f;

    [Header("Terrain Settings")]
    [SerializeField] private LevelOfDetailInfo[] detailLevels;
    [SerializeField] private int colliderLevelOfSimplificationIndex;
    [SerializeField] private static int maxViewDistance = 300;

    [Header("Customizable Asset Settings")]
    [SerializeField] private MeshSettings meshSettings;
    [SerializeField] private HeightMapSettings heightMapSettings;
    [SerializeField] private TextureData textureSettings;

    [Header("Map Material")]
    [SerializeField] private Material mapMaterial;

    [Header("Player Settings")]
    [SerializeField] private Transform player;
    [SerializeField] private static Vector3 playerPosition;

    [Header("Natural Elements Settings")]
    [SerializeField] private GameObject lTree;
    [SerializeField] private GameObject naturalOBJ;
    [SerializeField] private int maxNumberOfObjectsToSpawnPerEachChunk = 4;
    
    private Vector3 oldPlayerPosition;

    private float meshWorldSize;
    private int chunksVisibleInViewDistance;

    private Dictionary<Vector2, TerrainChunk> terrainChunkSpawned = new Dictionary<Vector2, TerrainChunk>();
    // Used to hide all the chunks that are now Out Of My Range
    private List<TerrainChunk> visibleTerrainChunks = new List<TerrainChunk>();

    public static int MaxViewDistance => maxViewDistance;

    public static Vector3 PlayerPosition { get => playerPosition; }
    public Transform Player { get => player; }
    public Vector3 OldPlayerPosition { get => oldPlayerPosition; }
    public List<TerrainChunk> VisibleTerrainChunks { get => visibleTerrainChunks; }

    public static float ColliderGenerationDistanceThreshold => colliderGenerationDistanceThreshold;

    private void Start()
    {
        textureSettings.ApplyToMaterial(mapMaterial);
        textureSettings.UpdateMeshHeights(mapMaterial, heightMapSettings.MinHeight, heightMapSettings.MaxHeight);

        maxViewDistance = detailLevels[detailLevels.Length - 1].visibleDistanceThreshold;
        meshWorldSize = meshSettings.MeshWorldSize;
        chunksVisibleInViewDistance = Mathf.RoundToInt(maxViewDistance / meshWorldSize);
        playerPosition = new Vector2(player.position.x, player.position.z) / meshSettings.meshScale;
        oldPlayerPosition = playerPosition;

        UpdateVisibleChunks();
    }

    private void Update()
    {
        // We scale the player position based on the scale of the map cos all the calculations that are done like the update visible chunk are done with that parameter and scaling only this prevents me to scale all the calculation that would change when we change scale
        playerPosition = new Vector2(player.position.x, player.position.z);

        if (playerPosition != oldPlayerPosition)
            foreach (TerrainChunk chunk in visibleTerrainChunks)
                chunk.UpdateCollisionMesh();

        if ((oldPlayerPosition - playerPosition).sqrMagnitude > squaredPlayerMovementThresholdBeforeChunkUpdate)
        {
            oldPlayerPosition = playerPosition;
            UpdateVisibleChunks();
        }
    }

    private void UpdateVisibleChunks()
    {
        int currentChunkCoordX = Mathf.RoundToInt(playerPosition.x / meshWorldSize);
        int currentChunkCoordZ = Mathf.RoundToInt(playerPosition.y / meshWorldSize);

        HashSet<Vector2> alraeadyUpdatedChunkCoords = new HashSet<Vector2>();

        // Going backwards so if we remove a chunk from the list we are not iterating through it again and so we will no loger get indexes errors
        for (int i = visibleTerrainChunks.Count - 1; i >= 0; i--)
        {
            alraeadyUpdatedChunkCoords.Add(visibleTerrainChunks[i].Coordinates);
            visibleTerrainChunks[i].UpdateTerrainChunk();
        }

        for (int yOffset = -chunksVisibleInViewDistance; yOffset <= chunksVisibleInViewDistance; yOffset++)
            for (int xOffset = -chunksVisibleInViewDistance; xOffset <= chunksVisibleInViewDistance; xOffset++)
            {
                // We spawn a chunk with a cerain xOffset around us, in order to give the world consistency
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordZ + yOffset);


                if (!alraeadyUpdatedChunkCoords.Contains(viewedChunkCoord))
                    if (terrainChunkSpawned.ContainsKey(viewedChunkCoord))
                        terrainChunkSpawned[viewedChunkCoord].UpdateTerrainChunk();
                    else
                    {
                        TerrainChunk terrainChunk = new TerrainChunk(viewedChunkCoord, heightMapSettings, meshSettings, detailLevels, colliderLevelOfSimplificationIndex,
                            this.transform, mapMaterial, meshSettings.meshScale);

                        terrainChunkSpawned.Add(viewedChunkCoord, terrainChunk);
                        terrainChunk.OnVisibilityChanged += OnTerrainChunkVisibilityChanged;
                        terrainChunk.Load();

                        StartCoroutine(LoadNaturalElementsDelayed(terrainChunk, 1f));
                    }

                NaturalElementsGenerator.LoadNaturalElements(naturalOBJ, lTree, terrainChunkSpawned[viewedChunkCoord], heightMapSettings, this.transform, maxNumberOfObjectsToSpawnPerEachChunk);
            }
    }

    private IEnumerator LoadNaturalElementsDelayed(TerrainChunk terrainChunk, float delay)
    {
        yield return new WaitForSeconds(delay);

        NaturalElementsGenerator.LoadNaturalElements(naturalOBJ, lTree, terrainChunk, heightMapSettings, this.transform, maxNumberOfObjectsToSpawnPerEachChunk);
    }

    private void OnTerrainChunkVisibilityChanged(TerrainChunk terrainChunk, bool isVisible)
    {
        if (isVisible)
            visibleTerrainChunks.Add(terrainChunk);
        else
            visibleTerrainChunks.Remove(terrainChunk);
    }
}
