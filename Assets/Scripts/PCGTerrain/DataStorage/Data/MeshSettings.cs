﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu()]
public class MeshSettings : UpdatableData
{
    public float meshScale = 2.5f;
    public bool useFlatShading;

    public const int numberOfSupportedLevelOfDetails = 5;
    public const int numOfSupportedChunkSizes = 9;
    public const int numOfSupportedFlatshaededChunkSizes = 3;
    public static readonly int[] supportedChunkSizes =
    {
        48,
        72,
        96,
        120,
        144,
        168,
        192,
        216,
        240
    };

    [Range(0, numOfSupportedChunkSizes - 1)] [SerializeField] private int chunkSizeIndex;
    [Range(0, numOfSupportedFlatshaededChunkSizes - 1)] [SerializeField] private int flatshadedChunkSizeIndex;

    // With flat shading we can generate a smaller map cos Unity vertex cap is fixed at 57k and with flat shading we exponentially increment the number of vertices
    /// <summary>
    /// This gives us the number of vertices per line rendered at level of simplification = 0. 
    /// This includes the extra two vertices that are excluded from the final mesh, but used for calculating the normals.
    /// </summary>
    public int NumberOfVerticesPerLine
    {
        get
        {
            return supportedChunkSizes[useFlatShading ? flatshadedChunkSizeIndex : chunkSizeIndex] + 5;
        }
    }

    public float MeshWorldSize
    {
        get
        {
            return (NumberOfVerticesPerLine - 3) * meshScale;
        }
    }
}
