﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class HeightMapSettings : UpdatableData
{
    public NoiseSettings noiseSettings;

    public bool applyFalloffMask;

    [Range(-15f, 15f)] [HideInInspector] public float paddingFalloffMaskFactorA = 3f;
    [Range(0.0f, 20f)] [HideInInspector] public float paddingFalloffMaskFactorB = 2.2f;

    public float heightMultiplier;
    public AnimationCurve heightCurve;


    public float MinHeight
    {
        get
        {
            return heightMultiplier * heightCurve.Evaluate(0);
        }
    }

    public float MaxHeight
    {
        get
        {
            return heightMultiplier * heightCurve.Evaluate(1);
        }
    }

#if UNITY_EDITOR

    protected override void OnValidate()
    {
        base.OnValidate();

        noiseSettings.ValidateValues();
    }

    #endif
}
