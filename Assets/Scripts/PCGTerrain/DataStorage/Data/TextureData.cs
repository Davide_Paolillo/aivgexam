﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu()]
public class TextureData : UpdatableData
{
    private const int textureSize = 512;
    private const TextureFormat textureFormat = TextureFormat.RGB565;

    [Header("Regions Color")]
    public TerrainLayer[] terrainLayers;

    private float savedMinHeight;
    private float savedMaxHeight;

    public void ApplyToMaterial(Material material)
    {
        material.SetInt("layerCount", terrainLayers.Length);
        material.SetColorArray("baseColors", terrainLayers.Select(x => x.tint).ToArray());
        material.SetFloatArray("baseStartHeights", terrainLayers.Select(x => x.startHeight).ToArray());
        material.SetFloatArray("baseBlends", terrainLayers.Select(x => x.blendStrength).ToArray());
        material.SetFloatArray("baseColorsStrength", terrainLayers.Select(x => x.tintStrength).ToArray());
        material.SetFloatArray("baseTextureScales", terrainLayers.Select(x => x.textureScale).ToArray());

        Texture2DArray texture2DArray = GenerateTextureArray(terrainLayers.Select(x => x.texture).ToArray());
        material.SetTexture("baseTextures", texture2DArray);

        // So if we change the shader code we reload the shader, so we can see the changes happening 
        UpdateMeshHeights(material, savedMinHeight, savedMaxHeight);
    }

    Texture2DArray GenerateTextureArray(Texture2D[] textures)
    {
        Texture2DArray texture2DArray = new Texture2DArray(textureSize, textureSize, textures.Length, textureFormat, true);

        for (int i = 0; i < textures.Length; i++)
        {
            texture2DArray.SetPixels(textures[i].GetPixels(), i);
        }

        texture2DArray.Apply();

        return texture2DArray;
    }

    public void UpdateMeshHeights(Material material, float minHeight, float maxHeight)
    {
        savedMaxHeight = maxHeight;
        savedMinHeight = minHeight;

        material.SetFloat("minHeight", minHeight);
        material.SetFloat("maxHeight", maxHeight);
    }
}
