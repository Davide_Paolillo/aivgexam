﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapesGenerator : MonoBehaviour
{
    [Header("Shape precision")]
    [Range(2, 256)][SerializeField] private int resolution = 10;

    [Header("First Shape")]
    [Range(0.0f, 100.0f)][SerializeField] private float a = 1.0f;
    [Range(0.0f, 100.0f)][SerializeField] private float b = 1.0f;
    [Range(-1000.0f, 1000.0f)][SerializeField] private float m = 1.0f;
    [Range(-1000.0f, 1000.0f)][SerializeField] private float n1 = 1.0f;
    [Range(-1000.0f, 1000.0f)][SerializeField] private float n2 = 1.0f;
    [Range(-1000.0f, 100.0f)][SerializeField] private float n3 = 1.0f;

    [Header("Rotational Structures")]
    [Range(0.0f, 1000.0f)][SerializeField] private float zeta = 1.0f;
    [Range(0.0f, 1000.0f)][SerializeField] private float delta = 1.0f;

    [Header("Second Shape")]
    [Range(0.0f, 100.0f)] [SerializeField] private float sa = 1.0f;
    [Range(0.0f, 100.0f)] [SerializeField] private float sb = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sm = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sn1 = 1.0f;
    [Range(-1000.0f, 1000.0f)] [SerializeField] private float sn2 = 1.0f;
    [Range(-1000.0f, 100.0f)] [SerializeField] private float sn3 = 1.0f;

    // Saving the meshes in the editor, don't need to show it
    [SerializeField, HideInInspector] private MeshFilter[] meshFilters;
    private SuperShape[] cubeFaces;
    private GameObject meshObject;

    // Draw in the editor
    private void OnValidate()
    {
        Initialize();
        GenerateMesh();
    }

    private void Update()
    {
        InitializeInUnpdate();
        GenerateMeshWithTime();
    }

    private void Initialize()
    {
        if(meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if(meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }

        //Destroy(meshObject.GetComponent<MeshCollider>());
        //meshObject.AddComponent<MeshCollider>().convex = true;
    }

    private void InitializeInUnpdate()
    {
        if (meshFilters == null)
        {
            // want to render 6 quads to build a quad
            meshFilters = new MeshFilter[1];
        }
        cubeFaces = new SuperShape[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            // So we avoid reinitializing everything too many times
            if (meshFilters[0] == null)
            {
                meshObject = new GameObject("mesh " + i.ToString());
                meshObject.transform.parent = this.gameObject.transform;
                meshObject.AddComponent<MeshCollider>().convex = true;

                meshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[0] = meshObject.AddComponent<MeshFilter>();
                meshFilters[0].sharedMesh = new Mesh();
            }

            // Specifying a different direction up for each face
            cubeFaces[i] = new SuperShape(meshFilters[0].sharedMesh, resolution, directions[i]);
        }

        if(meshObject != null)
        {
            Destroy(meshObject.GetComponent<MeshCollider>());
            meshObject.AddComponent<MeshCollider>().convex = true;
        }
        else
        {
            meshObject = this.transform.GetChild(0).gameObject;
            meshObject.AddComponent<MeshCollider>().convex = true;
        }
    }

    private void GenerateMesh()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, m, n1, n2, n3, sa, sb, sm, sn1, sn2, sn3);
        }
    }

    private void GenerateMeshWithTime()
    {
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructCompositeMesh(a, b, Time.time % 100, n1, n2, n3, sa, sb, Time.fixedTime % 100, sn1, sn2, sn3);
        }
    }

    private void GenerateMeshWithSin()
    {
        float sinValue = Mathf.Sin(Time.time) * 10.0f;
        float sinValue2 = Mathf.Sin(Time.fixedTime) * 10.0f;
        foreach (SuperShape face in cubeFaces)
        {
            face.ConstructGeneralizedMesh(a, b, sinValue, sinValue2, n1, n2, n3);
        }
    }
}
