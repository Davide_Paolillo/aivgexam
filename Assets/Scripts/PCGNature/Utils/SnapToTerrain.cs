﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToTerrain : MonoBehaviour
{

    public void UpdatePosition(Vector3 randomPosition, float yPadding)
    {
        RaycastHit hit = new RaycastHit();
        this.transform.position = new Vector3(randomPosition.x, 100f, randomPosition.z);

        if (Physics.Raycast(this.transform.position, -Vector3.up, out hit, Mathf.Infinity))
        {
            this.transform.position = new Vector3(this.transform.position.x, hit.point.y, this.transform.position.z);
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + yPadding, this.transform.position.z);
        }
    }
}
