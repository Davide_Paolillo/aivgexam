﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyInstantiator : MonoBehaviour
{
    [SerializeField] private int numberOfCopies = 5;
    [SerializeField] private int numberOfSplits = 2;

    [Header("Tree Generation Settings")]
    [SerializeField] private Vector3 angleOfCopy;
    [SerializeField] private float scaleDownFactor = .7f;

    public int NumberOfIterations { get => numberOfCopies; set => numberOfCopies = value; }

    private void Start()
    {
        if (numberOfCopies < 2)
            return;

        numberOfCopies -= 1;

        for (int i = 0; i < numberOfSplits; i++)
        {
            GameObject copy = Instantiate(this.gameObject);
            copy.transform.parent = this.transform.parent;
            copy.transform.localPosition = this.transform.localPosition;
            // So we get a rotation of -30 or 30
            Vector3 rotationAngle = new Vector3(angleOfCopy.x * RandomSign() + PaddingFactor(), angleOfCopy.y * RandomSign() + PaddingFactor(), angleOfCopy.z * RandomSign() + PaddingFactor());
            CopyRotatorHandler.RotateCopy(copy.transform, rotationAngle);
            CopyPositionHandler.TranslateCopy(copy.transform, this.transform.up * this.transform.localScale.y);
            CopyScaleHandler.ScaleCopy(copy.transform, scaleDownFactor);
        }
    }

    private float PaddingFactor()
    {
        return Random.Range(-10f, 10f);
    }

    private static int RandomSign()
    {
        return Random.Range(0f, 1f) >= 0.5f ? 1 : -1;
    }
}
