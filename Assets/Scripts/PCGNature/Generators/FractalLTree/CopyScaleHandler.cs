﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CopyScaleHandler
{
    public static void ScaleCopy(Transform copyTransform, float scaleDownFactor)
    {
        copyTransform.localScale *= scaleDownFactor;
    }
}
