﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CopyPositionHandler
{
    public static void TranslateCopy(Transform copyTransform, Vector3 positionToTranslate)
    {
        copyTransform.position += positionToTranslate;
    }
}
