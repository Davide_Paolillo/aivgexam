﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CopyRotatorHandler
{
    public static void RotateCopy(Transform copyTransform, Vector3 rotation)
    {
        copyTransform.rotation *= Quaternion.Euler(rotation);
    }
}
