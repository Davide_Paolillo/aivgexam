﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NaturalElementsGenerator
{
    public static void LoadNaturalElements(GameObject naturalOBJ, GameObject FractalLTreePrefab, TerrainChunk terrainChunk, HeightMapSettings heightMapSettings, Transform transform, int maxNumberOfObjectsToSpawnPerEachChunk)
    {
        HashSet<int> randomCoordinateForSpawningObjects = new HashSet<int>();
        Matrix4x4 localToWorld = transform.localToWorldMatrix;

        if (terrainChunk.MeshFilter.mesh.vertices.Length > 0 && terrainChunk.MeshCollider.sharedMesh != null && terrainChunk.IsVisible() && terrainChunk.MeshObject.transform.childCount < 1)
        {
            int objectsSpawnCount = Random.Range(1, maxNumberOfObjectsToSpawnPerEachChunk);
            for (int i = 0; i < objectsSpawnCount; i++)
            {
                int randomIndex = Random.Range(0, terrainChunk.MeshFilter.mesh.vertices.Length);

                if (randomCoordinateForSpawningObjects.Add(randomIndex))
                {
                    Vector3 randomPosition = localToWorld.MultiplyPoint3x4(terrainChunk.MeshFilter.mesh.vertices[randomIndex]) + terrainChunk.MeshObject.transform.position;

                    float vertexHeight = randomPosition.y / heightMapSettings.heightMultiplier;

                    if (Random.Range(0f, 1f) >= 0.5f && vertexHeight >= 0.102f)
                    {
                        GameObject lTree = GameObject.Instantiate(FractalLTreePrefab, Vector3.up * 100f, Quaternion.identity);
                        SnapToTerrain snap = lTree.AddComponent<SnapToTerrain>();
                        snap.UpdatePosition(randomPosition, 0f);
                        lTree.transform.parent = terrainChunk.MeshObject.transform;
                        lTree.AddComponent<Rigidbody>().isKinematic = true;
                    }
                    else
                    {
                        Vector3 randomDirection = transform.TransformDirection(terrainChunk.MeshFilter.mesh.normals[randomIndex]);

                        GameObject natureOBJ = GameObject.Instantiate(naturalOBJ, Vector3.up * 100f, Quaternion.identity);

                        int randomComponent = Random.Range(0, 6);

                        switch (randomComponent)
                        {
                            case 0:
                                GenerateSuperShape4D(natureOBJ, vertexHeight);
                                break;
                            case 1:
                                GenerateSuperShapeArchimedeanSpiral4D(natureOBJ, vertexHeight);
                                break;
                            case 2:
                                GenerateSuperShapeArchimedeanSpiralThoroidal4D(natureOBJ, vertexHeight);
                                break;
                            case 3:
                                GenerateSuperShapeLogarithmicSpiral4D(natureOBJ, vertexHeight);
                                break;
                            case 4:
                                GenerateSuperShapeLogarithmicSpiralThoroidal4D(natureOBJ, vertexHeight);
                                break;
                            case 5:
                                GenerateSuperShapeThoroidal4D(natureOBJ, vertexHeight);
                                break;
                            default:
                                GenerateSuperShapeArchimedeanSpiralThoroidal4D(natureOBJ, vertexHeight);
                                break;
                        }


                        float yPadding = natureOBJ.GetComponentInChildren<MeshFilter>().mesh.bounds.size.z;
                        natureOBJ.transform.Rotate(Vector3.right, 90.0f);
                        SnapToTerrain snapToTerrain = natureOBJ.AddComponent<SnapToTerrain>();
                        snapToTerrain.UpdatePosition(randomPosition, yPadding);
                        natureOBJ.transform.Rotate(randomDirection);
                        natureOBJ.transform.parent = terrainChunk.MeshObject.transform;
                        natureOBJ.AddComponent<Rigidbody>().isKinematic = false;
                    }
                }
            }
        }
    }

    private static void GenerateSuperShape4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShape4D superShape4D = natureOBJ.AddComponent<SuperShape4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.UpdateMesh();
    }

    private static void GenerateSuperShapeArchimedeanSpiral4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShapeArchimedeanSpiral4D superShape4D = natureOBJ.AddComponent<SuperShapeArchimedeanSpiral4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.UpdateMesh();
    }

    private static void GenerateSuperShapeLogarithmicSpiral4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShapeLogarithmicSpiral4D superShape4D = natureOBJ.AddComponent<SuperShapeLogarithmicSpiral4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.SpiralScaleFactor = 0.001f;
        superShape4D.UpdateMesh();
    }

    private static void GenerateSuperShapeArchimedeanSpiralThoroidal4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShapeArchimedeanSpiralThoroid4D superShape4D = natureOBJ.AddComponent<SuperShapeArchimedeanSpiralThoroid4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.UpdateMesh();
    }

    private static void GenerateSuperShapeLogarithmicSpiralThoroidal4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShapeLogarithmicSpiralThoroid4D superShape4D = natureOBJ.AddComponent<SuperShapeLogarithmicSpiralThoroid4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.SpiralScaleFactor = 0.001f;
        superShape4D.MaxLongitudeAngleInDegrees = 1200f;
        superShape4D.SpiralWrapFactor = 0.2f;
        superShape4D.UpdateMesh();
    }

    private static void GenerateSuperShapeThoroidal4D(GameObject natureOBJ, float vertexHeight)
    {
        SuperShapeThoroidal4D superShape4D = natureOBJ.AddComponent<SuperShapeThoroidal4D>();

        // TODO: find ranges where we get natural elements and assign it based on the vertex Height
        superShape4D.M = Random.Range(-20.0f, 20.0f);
        superShape4D.UpdateMesh();
    }

    private static void GenerateFractalLTree()
    {

    }
}
