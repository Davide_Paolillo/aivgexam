    -          2019.4.21f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `   ,                                                                                                                                                                            ŕyŻ                                                                                    MapGeneratorĆ  using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    // So we generate every single time a map that is 240 * 240, we choose 240 cos is a pretty high number wich is divisible for all the even number up to 12, and this scales well with our levelOfSimplification system
    private static int mapChunkSize = (int)MapVertexNumber.STANDARD;

    [Header("Map Settings")]
    [Range(0, 6)][SerializeField] private int editorLevelOfSimplification;
    [SerializeField] private TerrainType[] regions;
    [SerializeField] private DrawMode drawMode;

    [Header("Noise Settings")]
    [SerializeField] private int seed;
    [SerializeField] private float noiseScale;
    [SerializeField] private int octaves;
    [SerializeField] private float lacunarity;
    [Range(0f, 1f)][SerializeField] private float persistance;
    [SerializeField] private Vector2 startingOffset;
    [SerializeField] private NormalizeMode normalizeMode;
    [SerializeField] private float normalizeFactor;
    [SerializeField] private bool applyFalloffMask;
    [Range(-15f, 15f)][SerializeField] private float paddingFalloffMaskFactorA = 3f;
    [Range(0.0f, 20f)][SerializeField] private float paddingFalloffMaskFactorB = 2.2f;

    [Header("Mesh Settings")]
    [SerializeField] private float meshHeightMultiplier;
    [SerializeField] private AnimationCurve meshHeightCurve;

    [Header("Light Effects")]
    [SerializeField] private bool useFlatShading;

    [Header("Editor Settings")]
    [SerializeField] private bool autoUpdate;

    private float[,] falloffMap;

    Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue;
    Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue;

    public int MapWidth { get => mapChunkSize; }
    public int MapHeight { get => mapChunkSize; }
    public float NoiseScale { get => noiseScale; }
    public bool AutoUpdate { get => autoUpdate; }

    public static int MapChunkSize => mapChunkSize;

    private void Start()
    {
        falloffMap = FalloffGenerator.GenerateFalloffMap(MapChunkSize, paddingFalloffMaskFactorA, paddingFalloffMaskFactorB);

        // With flat shading we can generate a smaller map cos Unity vertex cap is fixed at 57k and with flat shading we exponentially increment the number of vertices
        if (useFlatShading)
            mapChunkSize = (int)MapVertexNumber.FLAT_SHADED;
        else
            mapChunkSize = (int)MapVertexNumber.STANDARD;

        mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>>();
        meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>>();
    }

    private void OnValidate()
    {
        falloffMap = FalloffGenerator.GenerateFalloffMap(MapChunkSize, paddingFalloffMaskFactorA, paddingFalloffMaskFactorB);

        // With flat shading we can generate a smaller map cos Unity vertex cap is fixed at 57k and with flat shading we exponentially increment the number of vertices
        if (useFlatShading)
            mapChunkSize = (int)MapVertexNumber.FLAT_SHADED;
        else
            mapChunkSize = (int)MapVertexNumber.STANDARD;

        if (lacunarity < 1)
            lacunarity = 1;
    }

    public void RequestMapData(Vector2 center, Action<MapData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(center, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MapDataThread(Vector2 center, Action<MapData> callback) 
    {
        MapData mapData = GenerateMapData(center);

        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }
    }
    
    public void RequestMeshData(MapData mapData, int levelOfSimplification, Action<MeshData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MeshDataThread(mapData, levelOfSimplification, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MeshDataThread(MapData mapData, int levelOfSimplification, Action<MeshData> callback) 
    {
        MeshData meshData = MeshGenerator.GenerateTerrainMesh(mapData.heightMap, meshHeightMultiplier, meshHeightCurve, levelOfSimplification, useFlatShading);

        lock (meshDataThreadInfoQueue)
        {
            meshDataThreadInfoQueue.Enqueue(new MapThreadInfo<MeshData>(callback, meshData));
        }
    }

    private void Update()
    {
        if (mapDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        
        if (meshDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
    }

    private MapData GenerateMapData(Vector2 center)
    {
        if (octaves <= 0)
            this.octaves = 1;

        float[,] noiseMap = Noise.GenerateNoiseMap(mapChunkSize + 2, mapChunkSize + 2, seed, noiseScale, octaves, persistance, lacunarity, center + startingOffset, normalizeMode, normalizeFactor);

        Color[] colorMap = new Color[mapChunkSize * mapChunkSize];

        for (int y = 0; y < mapChunkSize; y++)
        {
            for (int x = 0; x < mapChunkSize; x++)
            {
                if (applyFalloffMask)
                    noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - this.falloffMap[x, y]);

                float currentHeight = noiseMap[x, y];
                for (int i = 0; i < regions.Length; i++)
                {
                    if (currentHeight <= regions[i].height)
                    {
                        colorMap[y * mapChunkSize + x] = regions[i].color;
                        break;
                    }
                }
            }
        }

        return new MapData(noiseMap, colorMap);
    }

    public void DrawMap()
    {
        MapData mapData = GenerateMapData(Vector2.zero);

        MapDisplay mapDisplay = FindObjectOfType<MapDisplay>();
        switch (drawMode)
        {
            case DrawMode.COLOR_MAP:
                mapDisplay.DrawNoiseMap(mapData.heightMap, mapData.colorMap);
                break;
            case DrawMode.NOISE_MAP:
                mapDisplay.DrawNoiseMap(mapData.heightMap);
                break;
            case DrawMode.FALLOFF_MAP:
                mapDisplay.DrawFalloffMap(mapData.heightMap);
                break;
            case DrawMode.MESH:
                mapDisplay.GenerateMapMesh(MeshGenerator.GenerateTerrainMesh(mapData.heightMap, meshHeightMultiplier, meshHeightCurve, editorLevelOfSimplification, useFlatShading), TextureGenerator.TextureFromColorMap(mapData.colorMap, MapWidth, MapHeight));
                break;
            default:
                mapDisplay.DrawNoiseMap(mapData.heightMap);
                break;
        }
    }
}
                         MapGenerator    