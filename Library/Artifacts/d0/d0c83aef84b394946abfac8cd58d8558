    .          2019.4.21f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                      \       ŕyŻ     `   0                                                                                                                                                                            ŕyŻ                                                                                    MapGeneratorĚ  using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    // So we generate every single time a map that is 240 * 240, we choose 240 cos is a pretty high number wich is divisible for all the even number up to 12, and this scales well with our levelOfSimplification system
    private static int mapChunkSize;

    [Header("Map Settings")]
    [Range(0, MeshGenerator.numberOfSupportedLevelOfDetails)] [SerializeField] private int editorLevelOfSimplification;
    [SerializeField] private DrawMode drawMode;

    [Header("Terrain Settings")]
    [SerializeField] private TerrainData terrainData;
    [Range(0, MeshGenerator.numOfSupportedChunkSizes-1)] [SerializeField] private int chunkSizeIndex;
    [Range(0, MeshGenerator.numOfSupportedFlatshaededChunkSizes-1)] [SerializeField] private int flatshadedChunkSizeIndex;

    [Header("Texture Settings")]
    [SerializeField] private TextureData textureData;
    [SerializeField] private Material terrainMaterial;

    [Header("Noise Settings")]
    [SerializeField] private HeightMapSettings noiseData;
    [SerializeField] private float normalizeFactor;

    [Header("Editor Settings")]
    [SerializeField] private bool autoUpdate;

    private float[,] falloffMap;

    Queue<MapThreadInfo<HeightMap>> mapDataThreadInfoQueue;
    Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue;

    // With flat shading we can generate a smaller map cos Unity vertex cap is fixed at 57k and with flat shading we exponentially increment the number of vertices
    public int MapChunkSize 
    {
        get
        {
            if (terrainData.useFlatShading)
                mapChunkSize = MeshGenerator.supportedFlatshadedChunksizes[flatshadedChunkSizeIndex] - 1;
            else
                mapChunkSize = MeshGenerator.supportedChunkSizes[chunkSizeIndex] - 1;

            return mapChunkSize;
        }    
    }
    
    public int MapWidth { get => MapChunkSize; }
    public int MapHeight { get => MapChunkSize; }
    public float NoiseScale { get => noiseData.noiseScale; }
    public bool AutoUpdate { get => autoUpdate; }


    public TerrainData TerrainData { get => terrainData; }

    private void Awake()
    {
        textureData.ApplyToMaterial(terrainMaterial);
        textureData.UpdateMeshHeights(terrainMaterial, terrainData.MinHeight, terrainData.MaxHeight);
    }

    private void Start()
    {
        mapDataThreadInfoQueue = new Queue<MapThreadInfo<HeightMap>>();
        meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>>();
    }

    private void OnTextureValuesUpdated()
    {
        textureData.ApplyToMaterial(terrainMaterial);
    }

    private void OnValuesUpdated()
    {
        if (!Application.isPlaying)
            DrawMap();
    }

    private void OnValidate()
    {
        if (terrainData != null)
        {
            terrainData.OnValuesUpdated -= OnValuesUpdated;
            terrainData.OnValuesUpdated += OnValuesUpdated;
        }

        if (noiseData != null)
        {
            noiseData.OnValuesUpdated -= OnValuesUpdated;
            noiseData.OnValuesUpdated += OnValuesUpdated;
        }

        if (textureData != null)
        {
            textureData.OnValuesUpdated -= OnTextureValuesUpdated;
            textureData.OnValuesUpdated += OnTextureValuesUpdated;
        } 
    }

    private void InitializeFalloffMap()
    {
        falloffMap = FalloffGenerator.GenerateFalloffMap(MapChunkSize + 2, terrainData.paddingFalloffMaskFactorA, terrainData.paddingFalloffMaskFactorB);
    }

    public void RequestMapData(Vector2 center, Action<HeightMap> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(center, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MapDataThread(Vector2 center, Action<HeightMap> callback) 
    {
        HeightMap mapData = GenerateMapData(center);

        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<HeightMap>(callback, mapData));
        }
    }
    
    public void RequestMeshData(HeightMap mapData, int levelOfSimplification, Action<MeshData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MeshDataThread(mapData, levelOfSimplification, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MeshDataThread(HeightMap mapData, int levelOfSimplification, Action<MeshData> callback) 
    {
        MeshData meshData = MeshGenerator.GenerateTerrainMesh(mapData.values, terrainData.meshHeightMultiplier,
            terrainData.meshHeightCurve, levelOfSimplification, terrainData.useFlatShading);

        lock (meshDataThreadInfoQueue)
        {
            meshDataThreadInfoQueue.Enqueue(new MapThreadInfo<MeshData>(callback, meshData));
        }
    }

    private void Update()
    {
        if (mapDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<HeightMap> threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        
        if (meshDataThreadInfoQueue.Count > 0)
            for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
    }

    private HeightMap GenerateMapData(Vector2 center)
    {
        if (noiseData.octaves <= 0)
            this.noiseData.octaves = 1;


        float[,] noiseMap = Noise.GenerateNoiseMap(MapChunkSize + 2, MapChunkSize + 2, noiseData.seed, noiseData.noiseScale,
            noiseData.octaves, noiseData.persistance, noiseData.lacunarity,
                center + noiseData.startingOffset, noiseData.normalizeMode, normalizeFactor);

        if (terrainData.applyFalloffMask)
        {
            InitializeFalloffMap();

            for (int y = 0; y < MapChunkSize + 2; y++)
            {
                for (int x = 0; x < MapChunkSize + 2; x++)
                {
                    if (terrainData.applyFalloffMask)
                        noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - this.falloffMap[x, y]);
                }
            }
        }

        return new HeightMap(noiseMap);
    }

    public void DrawMap()
    {
        textureData.UpdateMeshHeights(terrainMaterial, terrainData.MinHeight, terrainData.MaxHeight);

        HeightMap mapData = GenerateMapData(Vector2.zero);

        MapDisplay mapDisplay = FindObjectOfType<MapDisplay>();
        switch (drawMode)
        {
            case DrawMode.NOISE_MAP:
                mapDisplay.DrawNoiseMap(mapData.values);
                break;
            case DrawMode.FALLOFF_MAP:
                mapDisplay.DrawFalloffMap(mapData.values);
                break;
            case DrawMode.MESH:
                mapDisplay.GenerateMapMesh(MeshGenerator.GenerateTerrainMesh(mapData.values, terrainData.meshHeightMultiplier,
                    terrainData.meshHeightCurve, editorLevelOfSimplification, terrainData.useFlatShading));
                break;
            default:
                mapDisplay.DrawNoiseMap(mapData.values);
                break;
        }
    }
}
                       MapGenerator    